#ifndef __LOOP_ACTION_HH__
#define __LOOP_ACTION_HH__
#include <string>

#include <json/json.hpp>
using json = nlohmann::json;
#include <ponder/pondertype.hpp>
#include <ponder/classbuilder.hpp>
#include <ponder/class.hpp>
class LoopAction {
public:
  LoopAction(const json &j):m_config(j) {};
  virtual ~LoopAction(){};
  virtual int execute(int step) = 0;
protected:
  json m_config;
};
PONDER_TYPE(LoopAction)

class SEND_TRIGGER:public LoopAction {
public:
  SEND_TRIGGER(const json &j):LoopAction(j) {};
  int execute(int step) {
    std::cout << "SEND_TRIGGER:" << step <<std::endl;
    return 0;
  } 
};
PONDER_TYPE(SEND_TRIGGER)

class SETUP_PARAM:public LoopAction {
public:
  SETUP_PARAM(const json &j):LoopAction(j) {};
  int execute(int step) {
    std::cout << "SETUP_PARAM:" << step <<std::endl;
    return 0;
  } 
};
PONDER_TYPE(SETUP_PARAM)

class CHANGE_BIN:public LoopAction {
public:
  CHANGE_BIN(const json &j):LoopAction(j) {};
  int execute(int step) {
    std::cout << "CHANGE_BIN step:" << step <<std::endl;
    return 0;
  } 
};
PONDER_TYPE(CHANGE_BIN)

class SETUP_MASK:public LoopAction {
public:
  SETUP_MASK(const json &j):LoopAction(j) {};
  int execute(int step) {
    std::cout << "SETUP_MASK step:" << step <<std::endl;
    return 0;
  } 
};
PONDER_TYPE(SETUP_MASK)
class CONFIGURE_MODULES_NO_ENABLE:public LoopAction {
public:
  CONFIGURE_MODULES_NO_ENABLE(const json &j):LoopAction(j) {};
  int execute(int step) {
    std::cout << "CONFIGURE_MODULE_NO_ENABLE step: " << step <<std::endl;
    return 0;
  } 
};
PONDER_TYPE(CONFIGURE_MODULES_NO_ENABLE)



PONDER_TYPE(json)
#endif




