#----------------------------------------------------------------
# Generated CMake target import file.
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "ponder::ponder" for configuration ""
set_property(TARGET ponder::ponder APPEND PROPERTY IMPORTED_CONFIGURATIONS NOCONFIG)
set_target_properties(ponder::ponder PROPERTIES
  IMPORTED_LOCATION_NOCONFIG "${_IMPORT_PREFIX}/lib/libponder.so"
  IMPORTED_SONAME_NOCONFIG "libponder.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS ponder::ponder )
list(APPEND _IMPORT_CHECK_FILES_FOR_ponder::ponder "${_IMPORT_PREFIX}/lib/libponder.so" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
