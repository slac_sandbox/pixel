# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/wittgen/demo/pixel/ponder/test/examples/main.cpp" "/home/wittgen/demo/pixel/ponder/test/examples/CMakeFiles/egtest.dir/main.cpp.o"
  "/home/wittgen/demo/pixel/ponder/test/examples/simple.cpp" "/home/wittgen/demo/pixel/ponder/test/examples/CMakeFiles/egtest.dir/simple.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/wittgen/demo/pixel/ponder/CMakeFiles/ponder.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
