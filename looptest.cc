#include "LoopAction.hh"
#include <fstream>
#include <vector>
#include <ponder/uses/runtime.hpp>

void init() {

  ponder::Class::declare<json>("json").constructor();
  ponder::Class::declare<SEND_TRIGGER>("SEND_TRIGGER").constructor<json>().function("execute",&SEND_TRIGGER::execute);
  ponder::Class::declare<SETUP_PARAM>("SETUP_PARAM").constructor<json>().function("execute",&SETUP_PARAM::execute);
  ponder::Class::declare<CHANGE_BIN>("CHANGE_BIN").constructor<json>().function("execute",&CHANGE_BIN::execute);
  ponder::Class::declare<SETUP_MASK>("SETUP_MASK").constructor<json>().function("execute",&SETUP_MASK::execute);
  ponder::Class::declare<CONFIGURE_MODULES_NO_ENABLE>("CONFIGURE_MODULES_NO_ENABLE").constructor<json>().function("execute",&CONFIGURE_MODULES_NO_ENABLE::execute);
}

void NestedLoop(json &j,unsigned int depth,unsigned int n) 
{

  json loop=j["loops"][std::to_string(depth)]; 
  std::cout << depth << "  " << n <<std::endl;
  unsigned int start=loop["loopParams"][0];
  unsigned int stop=loop["loopParams"][1];
  unsigned int step=loop["loopParams"][2];
  std::vector<ponder::UserObject> actionList;
  json actions=loop["loopActions"];
  for(unsigned int a=0;a<actions.size();a++) {
    const ponder::Class& metaclass = ponder::classByName(actions[a]);
    ponder::runtime::ObjectFactory factory(metaclass);
    ponder::UserObject action = factory.construct(j);
    actionList.push_back(action);
  }
  for(unsigned int l=start;l<stop;l+=step) {
    for(auto const& value: actionList) {
      //value.call("execute",ponder::Args(l));
      json loop_arg;
            ponder::runtime::FunctionCaller  caller(value.getClass().function("execute"));
            caller.call(value,l );
    }
    if(depth<n-1) NestedLoop(j,depth+1,n);
  }
}


int main(int argc, char *argv[]){
  init(); // TODO: clean up
  json j;
  std::ifstream file("scan.json");
  file >> j;
  std::cout << j.dump() << std::endl;
  unsigned int n=j["nLoops"];
  NestedLoop(j,0,n);
  const ponder::Class& metaclass = ponder::classByName("SEND_TRIGGER");
  ponder::runtime::ObjectFactory factory(metaclass);
  ponder::UserObject action = factory.construct(j);
  LoopAction *la=(LoopAction *) action.pointer();
  la->execute(4);

 return 0;
}
