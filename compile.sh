pushd $PWD
cd ponder
cmake3 .
make 
popd
g++ -std=c++11 -o looptest looptest.cc -I. -Iponder/include -Wl,--rpath=ponder -lponder -Lponder -g -DPONDER_USES_RUNTIME_IMPL
